
$/tree
.
├── input
    ├── combiNMT
    ├	└── data
    ├	    ├── dev.en
    ├	    ├── dev.sen
    ├	    ├── original98.txt
    ├	    ├── original995.txt
    ├	    ├── simple98.txt
    ├	    ├── simple995.txt
    ├	    ├── test.en
    ├	    ├── test.sen
    ├	    ├── train.en
    ├	    └── train.sen
    └── run_test
└── output      
    ├── datasets
    └── tables_and_plots
